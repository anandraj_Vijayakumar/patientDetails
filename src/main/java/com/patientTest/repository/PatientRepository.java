package com.patientTest.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.patientTest.model.PatientModel;



public interface PatientRepository extends JpaRepository<PatientModel , Long> {

}
