package com.patientTest.model;

import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "patientdetails")
@EntityListeners(AuditingEntityListener.class)
public class PatientModel {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long patientid ;
	
	@NotNull(message = "Name may not be null")
	private String patientfirstname;
	
	@NotNull(message = "Name may not be null")
	private String patientlastname;
	
	@NotNull(message = "Address may not be null")
	private String address;
	
	@NotNull
	private Long phonenumber;

	public Long getPatientid() {
		return patientid;
	}

	public void setPatientid(Long patientid) {
		this.patientid = patientid;
	}

	public String getPatientfirstname() {
		return patientfirstname;
	}

	public void setPatientfirstname(String patientfirstname) {
		this.patientfirstname = patientfirstname;
	}

	public String getPatientlastname() {
		return patientlastname;
	}

	public void setPatientlastname(String patientlastname) {
		this.patientlastname = patientlastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public Long getPhonenumber() {
		return phonenumber;
	}

	public void setPhonenumber(Long phonenumber) {
		this.phonenumber = phonenumber;
	}


		

}
