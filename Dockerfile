FROM openjdk:8
ADD target/patientTest.war patientTest.war
EXPOSE 8086
ENTRYPOINT ["java", "-war", "patientTest.war"]